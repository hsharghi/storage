<?php

namespace Tests\Feature;

use App\Models\User\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Webmozart\Assert\Assert;

class ProfileTest extends TestCase
{
    /**
     * Test update profile (user info) data
     *
     * @return void
     */
    public function testUpdateProfileInfo()
    {
        $response = $this->registerUser('test');
        $profileData = [
            'bio' => 'this is bio',
            'phone_number' => '+989131417184'
        ];

        $token = $response->json('token');
        $profile = $this
            ->withHeader('Authorization', 'Bearer '.$token)
            ->json('PATCH', route('profile.update'), $profileData);

        $this->assertEquals($profile->json('data.bio'), $profileData['bio']);
        $this->assertEquals($profile->json('data.phone_number'), $profileData['phone_number']);

        $this->deleteUser('test');

    }

    /**
     * Test upload profile avatar
     *
     * @return void
     */
    public function testUploadAvatar()
    {
        $response = $this->registerUser('test');
        $token = $response->json('token');

        Storage::fake('avatars');

        $response = $this
            ->withHeader('Authorization', 'Bearer '.$token)
            ->json('POST', route('profile.avatar'), [
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $this->assertArrayHasKey('avatar',$response->json());

        $avatar = basename($response->json(['avatar']));
        // Assert the file was stored...
        Storage::disk('avatar')->assertExists($avatar);

        $this->deleteUser('test');
        Storage::disk('avatar')->delete($avatar);

    }

}
