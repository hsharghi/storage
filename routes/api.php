<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
                 'prefix' => 'v1',
                 'middleware' => ['throttle:180,1', 'auth:api'],
                 'namespace' => 'API',
             ], function() {

    Route::post('avatar', 'ProfileController@avatar')->name('profile.avatar');
});

Route::post('upload/{token}', 'UploadController@upload')->prefix('v1');

Route::get('/', function() {
    return 'Storage API v. 1.0';
});

