<?php

namespace App\Listeners\Registration;

use App\Events\UserRegistered;
use App\Models\Setting;
use App\Models\User\UserInfo;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GenerateReferralCode implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $referralCode = null;
        do {
            $random = rand(1.1e6, 9.9e6);
            $referralCode = Setting::getSettingValue('referral_code_prefix', 'MB') . $random;
            $exists = UserInfo::query()->where('referral_code', $referralCode)->count();
        } while ($exists);

        $userInfo = new UserInfo;
        $userInfo->referral_code = $referralCode;

        $event->user->info()->save($userInfo);
    }
}
