<?php

namespace App\Listeners\Registration;

use App\Events\UserRegistered;
use App\Models\Subscription\SubscriptionType;
use App\Repositories\UserRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignSubscriptionPlan implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {

        $repo = new UserRepository($event->user);
        $plan = SubscriptionType::query()->orderBy('id')->first();
        $repo->assignSubscriptionPlan($plan);

    }
}
