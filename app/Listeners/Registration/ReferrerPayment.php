<?php

namespace App\Listeners\Registration;

use App\Events\UserRegistered;
use App\Repositories\UserRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReferrerPayment implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if ($event->code) {
            $repository = new UserRepository($event->user);
            $referrer = $repository->getReferrerByCode($event->code);
            $event->user->info->update([
                                           'referrer_id' => $referrer->id
                                       ]);
        }


    }
}
