<?php

namespace App\Listeners\Registration;

use App\Events\UserRegistered;
use Illuminate\Auth\Events\Registered;

class FireRegisteredEvent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        event(new Registered($event->user));
    }
}
