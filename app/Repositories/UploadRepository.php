<?php
/**
 * Created by PhpStorm.
 * User: hadi
 * Date: 2019-05-07
 * Time: 14:27
 */
namespace App\Repositories;

use App\Exceptions\StorageSizeException;
use App\Exceptions\UploadSizeException;
use Illuminate\Http\Request;

class UploadRepository {

    private $token;
    private $tokenData = null;

    public function __construct($token = null)
    {
        $this->token = $token;
    }

    private function parseToken() : ?array {

        if ($this->tokenData) {
            return $this->tokenData;
        }

        if ($data = unserialize(base64_decode($this->token))) {
            if (is_array($data)) {
                return $data;
            }
        }

        return null;
    }

    private function getMaxUploadSize()
    {
        $data = $this->parseToken();
        return isset($data['maxUploadSize']) ? $data['maxUploadSize'] : 0;
    }

    private function getTotalStorageSize()
    {
        $data = $this->parseToken();
        return isset($data['totalStorage']) ? $data['totalStorage'] : 0;
    }

    public function getStoragePath() {
        $data = $this->parseToken();
        $userId = isset($data['userId']) ? $data['userId'] : 0;
        $path = \Storage::disk('upload')->path($userId);

        return $path;
    }

    /**
     *
     * Validates storage size limit to user's subscription plan storage size
     *
     * @param Request $request
     * @return bool
     * @throws StorageSizeException
     */
    private function validateStorageSize(Request $request): bool {

        $dirSize = 0;
        $dirPath = $this->getStoragePath();
        if (\File::isDirectory($dirPath)) {
            foreach( \File::allFiles($this->getStoragePath()) as $file)
            {
                $dirSize += $file->getSize();
            }

            $uploadSize = $request->get('totalSize');

            $dirSize = ($dirSize + $uploadSize) / 1024 / 1024;
        }

        if ($dirSize > $this->getTotalStorageSize()) {
            throw new StorageSizeException();
        }

        return true;
    }

    /**
     *
     * Validates upload size to user's subscription plan upload size limit
     *
     * @param Request $request
     * @return bool
     * @throws UploadSizeException
     */
    private function validateUploadSize(Request $request): bool
    {

        $totalUploadSize = $request->get('totalSize') / 1024 / 1024;

        if ($totalUploadSize > $this->getMaxUploadSize()) {
            throw new UploadSizeException();
        }

        return true;
    }

    /**
     * @param Request $request
     * @return bool
     * @throws StorageSizeException|UploadSizeException
     */
    public function validateUpload(Request $request): bool
    {
        try {
            return $this->validateUploadSize($request) && $this->validateStorageSize($request);
        } catch (UploadSizeException $e) {
            throw $e;
        } catch (StorageSizeException $e) {
            throw $e;
        }
    }

}